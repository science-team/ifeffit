c{sys.h  -*-fortran-*- 
c system and build specific stuff goes here
c to be included in iff_config.f
       sysdir = '/usr/local/share/ifeffit'
       pgdev  = '/xserve'
       inifile= 'startup.iff  .ifeffit'
       build = '1.2.11d'//
     $   ' Copyright (c) 2008 Matt Newville, Univ of Chicago'
c}
