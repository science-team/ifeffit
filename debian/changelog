ifeffit (2:1.2.11d-12.6) unstable; urgency=medium

  * Non-maintainer upload.
  * Bug fix: "ftbfs with GCC-14", thanks to Matthias Klose for the report
    and Niko Tyni for the patch (Closes: #1075085).

 -- Roland Mas <lolando@debian.org>  Tue, 06 Aug 2024 13:28:23 +0200

ifeffit (2:1.2.11d-12.5) unstable; urgency=medium

  * Non-maintainer upload.
  * Source-only upload for migration to testing.

 -- Roland Mas <lolando@debian.org>  Thu, 22 Dec 2022 09:14:00 +0100

ifeffit (2:1.2.11d-12.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Move all packages to main.

 -- Roland Mas <lolando@debian.org>  Tue, 20 Dec 2022 17:59:49 +0100

ifeffit (2:1.2.11d-12.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Source-only upload for migration to testing.

 -- Roland Mas <lolando@debian.org>  Tue, 20 Dec 2022 12:16:40 +0100

ifeffit (2:1.2.11d-12.2) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Update standards version to 4.6.1, no changes needed.

  [ Roland Mas ]
  * Non-maintainer upload.
  * Migrate to main section, thanks to Adrian Bunk (Closes: #1025806).

 -- Roland Mas <lolando@debian.org>  Tue, 13 Dec 2022 09:52:43 +0100

ifeffit (2:1.2.11d-12.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for giza as a replacement for pgplot5.

 -- Roland Mas <lolando@debian.org>  Wed, 05 Oct 2022 14:22:29 +0200

ifeffit (2:1.2.11d-12) unstable; urgency=medium

  * Rebuild for perl 5.34 transition (Closes: #1005124)

 -- Neil Williams <codehelp@debian.org>  Tue, 08 Feb 2022 08:41:12 +0000

ifeffit (2:1.2.11d-11) unstable; urgency=medium

  * Reintroduce into Debian (Closes: #996203: ITP:
    ifeffit -- Interactive XAFS analysis program)
  * Drop Python support as Python3 version is not ready.
  * Port to debhelper-compat 13
  * Move to machine-readable copyright
  * Add Salsa CI
  * Remove unused build-deps
  * Provide a usable homepage link (Closes: #984536)
  * Update control for removal of Python elements

 -- Neil Williams <codehelp@debian.org>  Tue, 12 Oct 2021 10:03:32 +0100

ifeffit (2:1.2.11d-10.2) unstable; urgency=medium

  * Non-maintainer upload.

  [ gregor herrmann ]
  * Fix "FTBFS with perl 5.26": add unescaped-left-brace.patch to escape
    literal opening curly braces.  (Closes: #869436)

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 24 Jul 2017 14:06:47 +0200

ifeffit (2:1.2.11d-10.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Sourceful upload for transitions (Closes: #808307).

 -- Stuart Prescott <stuart@debian.org>  Sun, 06 Nov 2016 22:37:37 +1100

ifeffit (2:1.2.11d-10) unstable; urgency=medium

  * Rebuild for latest Perl version
  * Replace CPPFunction by rl_completion_func_t in src/cmdline/iffshell.c
    for readline 6.3 compatibility (Closes: #749353)
  * Set Compatibility level to 9
  * Update to Standards-Version 3.9.6 (no changes)

 -- Carlo Segre <segre@debian.org>  Sat, 10 Jan 2015 18:30:17 -0600

ifeffit (2:1.2.11d-9) unstable; urgency=low

  * Change Build-Depends to libpng-dev instead of libpng12-dev
    (Closes: #662376)
  * Update to Standards-Version 3.9.3 (no changes)

 -- Carlo Segre <segre@debian.org>  Wed, 04 Jul 2012 15:09:00 -0500

ifeffit (2:1.2.11d-8) unstable; urgency=low

  * [Colin Watson <cjwatson@ubuntu.com>]
      Link against -lreadline, not /usr/lib/libreadline.a (Closes: #648839)
  * Change gs-common Build-Dep to ghostcript
  * Build for Perl 5.14

 -- Carlo Segre <segre@debian.org>  Fri, 30 Dec 2011 14:23:35 -0600

ifeffit (2:1.2.11d-7) unstable; urgency=low

  * Rebuild for Perl 5.12 (Closes: #626584, #627748)
  * Transition to dh_python2 (Closes: #616846)
  * Upgrade to Standards-Version 3.9.2 (no changes)
  * Set Compatibility level to 8

 -- Carlo Segre <segre@debian.org>  Sun, 19 Jun 2011 09:58:20 -0500

ifeffit (2:1.2.11d-6) unstable; urgency=low

  * Clean up build rules to minimize the size of diff.gz
  * Switch to dpkg-source 3.0 (quilt) format
  * Rename perl wrapper package to libifeffit-perl (Closes: #575933)

 -- Carlo Segre <segre@debian.org>  Wed, 07 Apr 2010 07:11:10 -0500

ifeffit (2:1.2.11d-5) unstable; urgency=low

  * Incorporate Ubuntu patch for --install-layout=deb in debian/rules

 -- Carlo Segre <segre@debian.org>  Sun, 21 Mar 2010 22:45:03 -0500

ifeffit (2:1.2.11d-4) unstable; urgency=low

  * Remove the \newif\ifpdf definitions in the documentation tree and
    replace with \usepackage{ifpdf} (Closes: #573161)
  * Upgrade to Standards-Version 3.8.4 (no changes)

 -- Carlo Segre <segre@debian.org>  Sun, 21 Mar 2010 12:09:45 -0500

ifeffit (2:1.2.11d-3) unstable; urgency=low

  * Update Standards-Version to 3.8.3 (no changes)
  * Remove libreadline5-dev Build-Dep (Closes: #553785)
  * Clean up all lintian errors:
    - insert Education top category in python-ifeffit.desktop
    - remove dh_desktop invocation in debian/rules
    - add ${misc:Depends} in all binary packages

 -- Carlo Segre <segre@debian.org>  Sun, 01 Nov 2009 20:49:05 -0600

ifeffit (2:1.2.11d-2) unstable; urgency=low

  * Modified src/lib/iff_show.f to solve crashing when certain strings
    are too long.

 -- Carlo Segre <segre@debian.org>  Thu, 05 Mar 2009 01:09:11 -0600

ifeffit (2:1.2.11d-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@debian.org>  Wed, 04 Feb 2009 23:06:50 -0600

ifeffit (2:1.2.11c-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@debian.org>  Mon, 17 Nov 2008 13:37:57 -0600

ifeffit (2:1.2.11b-1) unstable; urgency=low

  * New upstream release
  * Update to Standards-Version 3.8.0 (no changes)

 -- Carlo Segre <segre@debian.org>  Wed, 12 Nov 2008 09:23:20 -0600

ifeffit (2:1.2.10a-5) unstable; urgency=low

  * Rebuild for Perl 5.10 (no changes)

 -- Carlo Segre <segre@debian.org>  Sun, 04 May 2008 14:09:45 -0500

ifeffit (2:1.2.10a-4) unstable; urgency=low

  * Add Build-Dep on texlive-latex-base.
  * Build for python 2.5.

 -- Carlo Segre <segre@debian.org>  Wed, 16 Apr 2008 00:49:23 -0500

ifeffit (2:1.2.10a-3) unstable; urgency=low

  * Recompile to remove runtime error in fortran libraries (symbol lookup error)
  * Put python-ifeffit in Section contrib/python
  * Put perl-ifeffit in Section contrib/perl
  * Upgrade to Standards-Version 3.7.3 (no changes)

 -- Carlo Segre <segre@debian.org>  Sat, 29 Mar 2008 20:50:05 -0500

ifeffit (2:1.2.10a-2) unstable; urgency=low

  * Remove explicit dependence on libc6 which is covered by
    build-essential.

 -- Carlo Segre <segre@debian.org>  Sun, 23 Sep 2007 23:46:29 -0500

ifeffit (2:1.2.10a-1) unstable; urgency=low

  * New upstream release
  * Insert Homepage: field in debian/control
  * Change to gfortran
  * Versioned Depends: on pgplot5 compiled with -lpng
  * Remove 'readline' source from tarball
  * Bump epoch to 2 because of erroneous release of 1.3.0

 -- Carlo Segre <segre@debian.org>  Thu, 20 Sep 2007 12:03:23 -0500

ifeffit (1:1.3.0-4) unstable; urgency=low

  * Added ifeffit dependency to perl-ifeffit (stupid oversight!)

 -- Carlo Segre <segre@debian.org>  Tue,  2 Jan 2007 00:29:32 -0600

ifeffit (1:1.3.0-3) unstable; urgency=low

  * Added Tag: field for Enrico Zini's testing
  * Modified descriptions for perl-ifeffit and python-ifeffit and
    corrected typo in initial paragraph (Closes: #382613)

 -- Carlo Segre <segre@debian.org>  Sat,  9 Sep 2006 00:43:57 -0500

ifeffit (1:1.3.0-2) unstable; urgency=low

  * Build-Dep on libc-dev for compatibility with all architectures
    (Closes: #386470)

 -- Carlo Segre <segre@debian.org>  Thu,  7 Sep 2006 22:05:31 -0500

ifeffit (1:1.3.0-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@debian.org>  Wed,  6 Sep 2006 07:24:28 -0500

ifeffit (1:1.2.9-2) unstable; urgency=low

  * Made changes in array allocation to reduce memory usage and speed
    performance.  Original defaults too large on maxsca, maxtxt and
    mconst in src/lib/consts.h

 -- Carlo Segre <segre@debian.org>  Thu, 10 Aug 2006 10:20:30 -0500

ifeffit (1:1.2.9-1) unstable; urgency=low

  * First Debian release (Closes: #373207)
  * New upstream release
  * Cleaning up debian/rules file syntax
  * Adding pycentral support for compliance with new Python policy
  * Adding ifeffit-doc package with examples

 -- Carlo Segre <segre@debian.org>  Wed, 28 Jun 2006 10:55:53 -0500

ifeffit (1:1.2.8-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Fri, 16 Sep 2005 14:14:00 -0500

ifeffit (1:1.2.7a-1) unstable; urgency=low

  * New upstream release
  * Change to standards version 3.6.2

 -- Carlo Segre <segre@iit.edu>  Fri, 29 Jul 2005 00:02:11 -0500

ifeffit (1:1.2.7-2) unstable; urgency=low

  * Removed all python versioning because of python-pmw being pinned
    to the default python version.

 -- Carlo Segre <segre@iit.edu>  Tue,  7 Jun 2005 22:17:49 -0500

ifeffit (1:1.2.7-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Tue, 12 Apr 2005 22:23:19 -0500

ifeffit (1:1.2.6a-2) unstable; urgency=low

  * Bring Ifeffit.pm (perl-ifeffit package) up to version 1.301 as in horae.

 -- Carlo Segre <segre@iit.edu>  Thu,  2 Dec 2004 00:14:12 -0600

ifeffit (1:1.2.6a-1) unstable; urgency=low

  * New upstream release, allows for up to 99 data sets may be written out.

 -- Carlo Segre <segre@iit.edu>  Sun, 14 Nov 2004 18:55:23 -0600

ifeffit (1:1.2.6-2) unstable; urgency=low

  * Uncommented lines in feff.f to permit output of chi files

 -- Carlo Segre <segre@iit.edu>  Tue, 26 Oct 2004 17:57:55 -0500

ifeffit (1:1.2.6-1) unstable; urgency=low

  * New upstream release.

 -- Carlo Segre <segre@iit.edu>  Sat, 10 Jul 2004 11:10:34 -0500

ifeffit (1:1.2.5-4) unstable; urgency=low

  * Fixed some build dependencies.
  * Removed python declaration in scripts where they are not necessary
    to satisfy lintian.
  * Removed ifeffit_shell.pl as upstream author says it is not really
    part of the package!

 -- Carlo Segre <segre@iit.edu>  Wed,  9 Jun 2004 13:03:11 -0500

ifeffit (1:1.2.5-3) unstable; urgency=low

  * Fix build dependencies to make it build with pbuilder.
  * Upgrade to Standards-Version 3.6.1.

 -- Carlo Segre <segre@iit.edu>  Sat, 24 Apr 2004 12:00:23 -0500

ifeffit (1:1.2.5-2) unstable; urgency=low

  * Made packages more lintian compliant.

 -- Carlo Segre <segre@iit.edu>  Sun, 14 Mar 2004 15:52:20 -0600

ifeffit (1:1.2.5-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Sat, 28 Feb 2004 17:59:24 -0600

ifeffit (1:1.2.4-6) unstable; urgency=low

  * Added manpages and documentation.

 -- Carlo Segre <segre@iit.edu>  Sat, 10 Jan 2004 19:42:31 -0600

ifeffit (1:1.2.4-5) unstable; urgency=low

  * Removed compiled *.pyc and *.pyo files form package and have them
    generated and removed in the postinst prerm scripts.

 -- Carlo Segre <segre@iit.edu>  Fri,  9 Jan 2004 09:56:25 -0600

ifeffit (1:1.2.4-4) unstable; urgency=low

  * Added build for perl wrapper

 -- Carlo Segre <segre@iit.edu>  Sun, 21 Dec 2003 11:30:33 -0600

ifeffit (1:1.2.4-3) unstable; urgency=low

  * Fixed dependencies on python-tk

 -- Carlo Segre <segre@iit.edu>  Sat, 20 Dec 2003 10:29:38 -0600

ifeffit (1:1.2.4-2) unstable; urgency=low

  * Added build for python wrapper and gifeffit

 -- Carlo Segre <segre@iit.edu>  Fri, 19 Dec 2003 16:38:22 -0600

ifeffit (1:1.2.4-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Thu, 30 Oct 2003 23:28:57 -0600

ifeffit (1:1.2.3-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Sat, 27 Sep 2003 22:33:34 -0500

ifeffit (1:1.2.1-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Tue, 20 May 2003 12:11:41 -0500

ifeffit (1:1.2.0-1) unstable; urgency=low

  * New upstream release.

 -- Carlo Segre <segre@iit.edu>  Thu, 15 May 2003 11:08:52 -0500

ifeffit (1.0077-1) unstable; urgency=low

  * New upstream release, added epoch because of versioning change.

 -- Carlo Segre <segre@iit.edu>  Tue,  6 May 2003 00:13:36 -0500

ifeffit (1.0076-1) unstable; urgency=low

  * Initial Release.
  * Adjusted to find pgplot5 libraries

 -- Carlo Segre <segre@iit.edu>  Tue,  4 Mar 2003 21:01:36 -0600
