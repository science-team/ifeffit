Source: ifeffit
Section: science
Priority: optional
Maintainer: Debian PaN Maintainers <debian-pan-maintainers@alioth-lists.debian.net>
Uploaders:
 Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>,
 Picca Frédéric-Emmanuel <picca@debian.org>,
 Carlo Segre <segre@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 gfortran,
 ghostscript,
 libreadline-dev,
 giza-dev,
 latex2html,
 texlive-base,
 texlive-latex-base,
 chrpath,
Homepage: https://cars.uchicago.edu/ifeffit/Documentation
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/science-team/ifeffit
Vcs-Git: https://salsa.debian.org/science-team/ifeffit.git

Package: ifeffit
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: ifeffit-doc
Description: Interactive XAFS analysis program
 IFEFFIT is an interactive program for XAFS analysis. It combines the
 high-quality analysis algorithms of AUTOBK and FEFFIT with graphical display
 of XAFS data and general data manipulation.
 .
 IFEFFIT comes as a command-line program, but the underlying functionality is
 available as a programming library. The IFEFFIT library can be used from C,
 Fortran, Tcl, and Perl.

Package: ifeffit-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: ifeffit
Description: IFEFFIT examples and documentation
 This package contains all the available documentation for IFEFFIT and
 its component programs.  Example files for EXAFS data analysis are also
 provided as tutorials.

Package: libifeffit-perl
Architecture: any
Section: perl
Provides: perl-ifeffit
Replaces: perl-ifeffit
Conflicts: perl-ifeffit
Depends: ${shlibs:Depends}, ${perl:Depends}, ${misc:Depends}
Description: Perl extensions for IFEFFIT
 IFEFFIT is an interactive program for XAFS analysis. It combines the
 high-quality analysis algorithms of AUTOBK and FEFFIT with graphical display
 of XAFS data and general data manipulation.
 .
 This package provides an interface to IFEFFIT from Perl.
